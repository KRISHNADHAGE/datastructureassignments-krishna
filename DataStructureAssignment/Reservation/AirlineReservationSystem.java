package Com.Assignment.Reservation;
import java.util.Scanner;

public class AirlineReservationSystem {

    private boolean seatsBooked;
    private int CAPACITY = 10;
    private int availableSeats = CAPACITY;
    private boolean seatingCapacity[] = new boolean[CAPACITY];
    private Scanner input;
    private int userChoice;
    private int current;

    // Initialize all the elements of the array to false for all empty seats.
    public void initialiseSeats() {
        for (int i = 0; i < seatingCapacity.length; i++) {
            seatingCapacity[i] = false;
        }
    }

    public void displayMenu(){
//    	int avialable = CAPACITY-1;
        System.out.println("***** Airline Reservation System *****");
        System.out.println(" Menu Options ");
        System.out.println(" 1 - �smoking� ");
        System.out.println(" 2 - �non-smoking� ");
       
        
    }

    public void seatingChoice(){



        do{
            System.out.println("**************************************");
            System.out.printf("Please choose your option: ");
            input = new Scanner(System.in);
            // get the user choice
            userChoice = input.nextInt();
            while (userChoice != 1 && userChoice != 2 ) {
                System.out.println("Please choose either '1' or '2'");
            }

            seatsBooked = assignSeats(userChoice);

        }while(seatsBooked);

    }

    // Assign the seats based from the user's choice for the class
    public boolean assignSeats(int userChoice) {

        switch (userChoice) {

        // �smoking� section
        case 1 :

            for (current = 0; current < 5; current++) {
                if (seatingCapacity[current] == false) {
                    seatingCapacity[current] = true;
                    printBoardingPass(current + 1);
                    availableSeats--;
                    seatsBooked = true;
                    break;
                }
            }

            // if �smoking� section is full, prompt user to choose other class
            if (current == 5) {
                chooseOtherClass();
            }
            break;

            // �non-smoking� class section
        case 2:
            for (current = 5; current < 10; current++) {
                if (seatingCapacity[current] == false) {
                    seatingCapacity[current] = true;
                    printBoardingPass(current + 1);
                    availableSeats--;
                    seatsBooked = true;
                    break;
                }
            }

            // if �smoking� class section is full, prompt user to choose other class
            if (current == 10) {
                chooseOtherClass();
            }
            break;

        default:
            System.out.println("Invalid input. Please type again");
            seatsBooked = true;
        }
        return seatsBooked;

    }

    // Print the boarding pass with the seat number and class section
    public void printBoardingPass(int seatNumber) {

        System.out.println("***** Boarding Pass *****");
        System.out.println();
        System.out.println("Seat number: " +(seatNumber));
        
        if (seatNumber <= 5) {
            System.out.println("You've been assigned to �smoking�.");
        } else {
            System.out.println("You've been assigned to �non-smoking�.");
        }
        System.out.println();
        System.out.println("Available Seat:"+ availableSeats);
        System.out.println("*************************");
        System.out.println();

    }

    // If the chosen class is full, ask the user whether want to choose other class section
    private boolean chooseOtherClass() {
        do {
            System.out.println("There are no more seats available for the chosen option.");
            System.out.println("Do you want to choose another option? Type 'Y' for Yes or 'N' for No");

            String ans = input.next();
            if (ans.equalsIgnoreCase("Y")) {
                seatsBooked = true;
            } else if (ans.equalsIgnoreCase("N")) {
                System.out.println("Next flight leaves in 3 hours.");
                System.out.println();
                break;
            } else {
                System.out.println("Invalid input.");
            }

        } while (!seatsBooked);
        return seatsBooked;
        
        
        

    }

}