package Com.Assignment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmployeeDB {

	List<Employee>employeedb=new ArrayList<Employee>();
	public boolean addEmployee(Employee e) {
		return employeedb.add(e);
	}
	
	public boolean deleteEmployee(int empId) {
	 boolean isRemoved = false;
	//return isRemoved;
	 Iterator<Employee> itr=employeedb.iterator();
	 
	 while(itr.hasNext()) {
		 Employee emp=itr.next();
		 if(emp.getEmpId()==empId) {
			 isRemoved=true;
			 itr.remove();
		 }
	 }
	return isRemoved;
}
	public String showPaySlip(int empId) {
		String paySlip ="Invalid employee id";
		
		for(Employee e : employeedb) {
			if(e.getEmpId()==empId) {
				paySlip="Payslip for employee id"+empId+"is"+e.getEmpSalary();
	     }
		}
		return paySlip;
	}

	public Employee[] listAll() {
		Employee[] empArray =new Employee[employeedb.size()];
		for(int i=0;i<employeedb.size();i++)
		empArray[i]=employeedb.get(i);
		return empArray;
	}

}
