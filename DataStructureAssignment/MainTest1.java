package Com.Assignment;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class MainTest1 {

	public ArrayList<Integer> saveEvenNumbers(Integer N) {
		ArrayList<Integer> A1 = new ArrayList<Integer>();

		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0)
				A1.add(i);
		}
		return A1;
	}

	public ArrayList<Integer> printEvenNumbers(Integer N) {
		ArrayList<Integer> A2 = new ArrayList<Integer>();
		for (Integer integer : saveEvenNumbers(N)) {
			A2.add(integer * 2);
		}
		String s = A2.stream().map(t -> t + "").collect(Collectors.joining(", "));
		System.out.println(s);
		return A2;
	}

	public int printEvenNumber(Integer N) {
		String a1 = saveEvenNumbers(N).getClass().toString();

		if (a1.equals("class java.util.ArrayList"))
			return 1;
		else
			return 0;
	}

	public static void main(String[] args) {

		MainTest1 mt = new MainTest1();
		System.out.println("check the ArrayList available or not " + mt.printEvenNumber(10));

		System.out.println("Print the even numbers " + mt.saveEvenNumbers(10));

		System.out.print("number multiplied by 2 is :");
		mt.printEvenNumbers(10);

	}

}
