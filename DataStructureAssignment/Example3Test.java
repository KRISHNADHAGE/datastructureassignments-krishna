package Com.Assignment;

public class Example3Test {

	public static void main(String[] args) {
		EmployeeDB empbd=new EmployeeDB();

		Employee emp1=new Employee(2,"ravi","abc@gmail.com",'m',12000);
		Employee emp2=new Employee(3,"ram","abc@gmail.com",'m',14000);
		Employee emp3=new Employee(4,"raju","raju@gmail.com",'m',11000);
		Employee emp4=new Employee(5,"jrish","jrish@gmail.com",'m',1000);
		
		empbd.addEmployee(emp1);
		empbd.addEmployee(emp3);
		
		empbd.addEmployee(emp3);
		empbd.addEmployee(emp4);
		
		for(Employee emp : empbd.listAll())
			System.out.println(emp.GetEmployeeDetails());
		System.out.println();
		 empbd.deleteEmployee(2);
		 
		 for(Employee emp :empbd.listAll())
			 System.out.println(emp.GetEmployeeDetails());
		 
		 System.out.println();
		System.out.println(empbd.showPaySlip(3));
		
			
		}

	}


   